// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
    name: "CloudPayments",
    platforms: [
        .iOS(.v11),
    ],
    products: [
        .library(
            name: "CloudPayments",
            targets: ["CloudPayments"]
        )
    ],
    targets: [
        .target(
            name: "CloudPayments",
            dependencies: [
                "CloudpaymentsNetworking"
            ],
            path: "sdk/sdk/",
            exclude: [
                "Info.plist"
            ],
            resources: [
                .process("Resources")
            ]
        ),
        .target(
            name: "CloudpaymentsNetworking",
            path: "networking/source/"
        )
    ]
)
